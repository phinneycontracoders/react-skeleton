import React from 'react';
import { Typography } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import MainContainer from './MainContainer';
import PrimaryButton from './PrimaryButton';
import Form from './Form';
import { FileInput } from './FileInput';
import { useSelector, useDispatch } from 'react-redux';
import { addStepThreeData, getStepThreeData } from '../store/stepThreeSlice';

export const Step3 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const data = useSelector(getStepThreeData);

  const { control, handleSubmit } = useForm({
    // These will be what auto-populates in the fields where name=
    defaultValues: {
      files: data.files,
    },
  });

  const onSubmit = (data) => {
    dispatch(addStepThreeData(data));
    history.push('/result');
  }

  return (
    <MainContainer>
      <Typography
        component='h2'
        variant='h5'
      >
        Step 3
      </Typography>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <FileInput
          name='files'
          control={control}
        />
        <PrimaryButton>Next</PrimaryButton>
      </Form>
    </MainContainer>
  )
}