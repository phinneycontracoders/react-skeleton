import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './header';
import { Step1 } from './step1';
import { Step2 } from './step2';
import { Step3 } from './step3';
import { Result } from './Result';

function FormWrapper() {
  return (
    <>
    <Header />
    <Router>
      <Switch>
        <Route exact path='/' component={Step1} />
        <Route path='/step2' component={Step2} />
        <Route path='/step3' component={Step3} />
        <Route path='/result' component={Result} />
      </Switch>
    </Router>
    </>
  )
}

export default FormWrapper;
