import React from 'react';
import { Controller } from 'react-hook-form';
import Dropzone from 'react-dropzone';
import { Paper, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { CloudUpload, InsertDriveFile } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#eee',
    textAlign: 'center',
    cursor: 'pointer',
    padding: theme.spacing(2),
    color: 'deeppink',
    marginTop: theme.spacing(2),
  },
  icon: {
    marginTop: theme.spacing(2),
    color: '#888888',
    fontSize: '42px',
  },
}));

export const FileInput = ({ control, name, value }) => {
  const styles = useStyles();

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={[]}
      render={({ onChange, onBlur, value }) => (
        <>
          <Dropzone onDrop={onChange}>
            {({ getRootProps, getInputProps }) => (
              <Paper
                variant='outlined'
                className={styles.root}
                {...getRootProps()}
              >
                <CloudUpload className={styles.icon} />
                <input {...getInputProps()} name={name} onBlur={onBlur} />
              </Paper>
            )}
          </Dropzone>
          <List>
            {value.map((file, index) => (
              <ListItem key={index}>
                <ListItemIcon />
                <InsertDriveFile />
                <ListItemText
                  primary={file.name}
                  secondary={file.size}
                />
              </ListItem>
            ))}
          </List>
        </>
      )}
    >
    </Controller>
  )
}


