import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    alignItems: 'center',
  }
}))

const Form = ({ children, ...props }) => {
  const styles = useStyles();

  return (
    <form
      className={styles.root}
      noValidate {...props}
      >
        { children }
      </form>
  )
}

export default Form;    