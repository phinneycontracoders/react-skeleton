import React from 'react';
import MainContainer from './MainContainer';
import { Paper, Typography, TableContainer, Table, TableBody, TableHead, TableRow, TableCell, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
// import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { getStepOneData } from '../store/stepOneSlice';
import { getStepTwoData } from '../store/stepTwoSlice';
import { getStepThreeData } from '../store/stepThreeSlice';
import { InsertDriveFile } from '@material-ui/icons';
import PrimaryButton from './PrimaryButton';

// const useStyles = makeStyles({
//   root: {
//     marginBottom: theme.spacing(4),
//   },
//   table: {
//     marginBottom: theme.spacing(4),
//   },
// });

export const Result = () => {
  // const styles = useStyles();

  const stepOneData = useSelector(getStepOneData);
  const stepTwoData = useSelector(getStepTwoData);
  const stepThreeData = useSelector(getStepThreeData);

  const stepOneEntries = Object.entries(stepOneData).filter((entry) => entry);
  const stepTwoEntries = Object.entries(stepTwoData).filter((entry) => entry);
  const stepThreeEntries = Object.entries(stepThreeData).filter((entry) => entry);

  // NOTE: This is where the data would get sent to the database
  const onSubmit = async () => {
    // dispatches back to redux stores for steps 1, 2, 3 to make a call to save to the database
    console.log('this is where a call to the database would be made, but it is not happening yet...');
  }

  return (
    <MainContainer>
      <Typography component='h2' variant='h5'>
        Form Values
      </Typography>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Field</TableCell>
              <TableCell align='right'>Value</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {stepOneEntries.map((entry) => (
              <TableRow>
                <TableCell>
                  {entry[0]}
                </TableCell>
                <TableCell>
                  {entry[1].toString()}
                </TableCell>
              </TableRow>
            ))}

            {stepTwoEntries.map((entry) => (
              <TableRow>
                <TableCell>
                  {entry[0]}
                </TableCell>
                <TableCell>
                  {entry[1].toString()}
                </TableCell>
              </TableRow>
            ))}

          </TableBody>
        </Table>
      </TableContainer>

      {stepThreeEntries && (
        <>
        <Typography component='h2' variant='h5'>
          Files
        </Typography>
        {stepThreeEntries.map((file, idx) => (
          <ListItem key={idx}>
            <ListItemIcon>
              <InsertDriveFile />
            </ListItemIcon>
            <ListItemText 
            primary={file[1].name}
            secondary={file[1].size}
            />
          </ListItem>
        ))}
        </>
      )}
      <PrimaryButton onClick={onSubmit}>Submit</PrimaryButton>
      <Link to='/'>StartOver</Link>
    </MainContainer>
  )
}