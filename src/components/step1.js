import React from 'react';
import { Typography } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import MainContainer from './MainContainer';
import PrimaryButton from './PrimaryButton';
import Input from './Input';
import Form from './Form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useSelector, useDispatch } from 'react-redux';
import { addStepOneData, getStepOneData } from '../store/stepOneSlice';

const schema = yup.object().shape({
  firstName: yup
    .string()
    .matches(/^([^0-9]*)$/, 'First name should not contain numbers')
    .required('First name is a required field'),
  lastName: yup
    .string()
    .matches(/^([^0-9]*)$/, 'Last name should not contain numbers')
    .required('Last name is a required field'),
})

export const Step1 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const userAnswers = useSelector(getStepOneData);

  const { register, handleSubmit, errors } = useForm({
    // These will be what auto-populates in the fields where name=
    defaultValues: {
      firstName: userAnswers.firstName,
      lastName: userAnswers.lastName,
    },
    mode: 'onBlur',
    resolver: yupResolver(schema)
  });

  const onSubmit = (data) => {
    dispatch(addStepOneData(data));
    history.push('/step2');
  }

  return (
    <MainContainer>
      <Typography
      component='h2'
      variant='h5'
      >
        Step 1
      </Typography>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Input
        ref={register}
        name='firstName'
        type='text'
        label='First Name'
        // NOTE: IF userAnswers = truthy, show .firstName, else show 'First Name' ... but NOT a good thing to do with a label
        // label={userAnswers?.firstName || 'First Name'}
        error={!!errors.firstName}
        // NOTE: The !! (double bang) logical operators return a value's truthy value
        helperText={errors?.firstName?.message}
        // NOTE: IF errors exists, and IF firstName exists on it. If any of the objects (errors, firstName) return undefined, the whole thing will return undefined, instead of throwing errors
        />
        <Input
        ref={register}
        name='lastName'
        type='text'
        label='Last Name'
        error={!!errors?.lastName?.message}
        helperText={errors?.lastName?.message}
        />
        <PrimaryButton type='submit'>Next</PrimaryButton>
      </Form>
    </MainContainer>
  )
}

