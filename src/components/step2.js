import React from 'react';
import { FormControlLabel, Typography, Checkbox } from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import MainContainer from './MainContainer';
import PrimaryButton from './PrimaryButton';
import Input from './Input';
import Form from './Form';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useSelector, useDispatch } from 'react-redux';
import { addStepTwoData, getStepTwoData } from '../store/stepTwoSlice';

const schema = yup.object().shape({
  email: yup
    .string()
    .email('Email needs to have correct format')
    .required('Email is a required field'),
});

const normalizePhoneNumber = (value) => {
  const phoneNumber = parsePhoneNumberFromString(value);
  if (!phoneNumber) {
    return value;
  }
  return (
    phoneNumber.formatInternational()
  );
};

export const Step2 = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const userAnswers = useSelector(getStepTwoData);

  const { register, handleSubmit, watch, errors } = useForm({
    // These will be what auto-populates in the fields where name=
    defaultValues: {
      email: userAnswers.email,
      hasPhone: userAnswers.hasPhone,
      phoneNumber: userAnswers.phoneNumber,
    },
    mode: 'onBlur',
    resolver: yupResolver(schema)
  });

  const hasPhone = watch('hasPhone');

  const onSubmit = (data) => {
    dispatch(addStepTwoData(data));
    history.push('/step3');
  }

  return (
    <MainContainer>
      <Typography
        component='h2'
        variant='h5'
      >
        Step 2
      </Typography>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Input
          ref={register}
          type='email'
          label='Email'
          name='email'
          error={!!errors.email}
          helperText={errors?.email?.message}
          required
        />

        <FormControlLabel
          control={
            <Checkbox
              defaultValue={hasPhone}
              // defaultChecked={hasPhone}
              color='primary'
              inputRef={register}
              name='hasPhone'
            />
          }
          label='Do you have a phone?'
        />
        {hasPhone && (
          <Input
            ref={register}
            id='phoneNumber'
            type='tel'
            label='Phone Number'
            name='phoneNumber'
            onChange={(event) => {
              event.target.value = normalizePhoneNumber(event.target.value);
            }}
          />
        )}
        <PrimaryButton>Next</PrimaryButton>
      </Form>
    </MainContainer>
  )
}