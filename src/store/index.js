import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';
import storageSession from 'redux-persist/lib/storage/session'; // otherwise, defaults to localStorage /lib/storage

import stepOneReducer from './stepOneSlice';
import stepTwoReducer from './stepTwoSlice';
import stepThreeReducer from './stepThreeSlice';

const rootReducer = combineReducers({
  // name it will have in state : reducer being used to get such data
  stepOneData: stepOneReducer,
  stepTwoData: stepTwoReducer,
  stepThreeData: stepThreeReducer,
})

const persistConfig = {
  key: 'root',
  version: 1,
  storage: storageSession,
  whitelist: ['stepThreeData', 'stepOneData'],  // the ONLY data that will get persisted between page reloads (and for quite some time after until we tell it to purge - versus: 
  blacklist: ['stepTwoData'] // data that will NOT get persisted between page reloads
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
});

export default store;
