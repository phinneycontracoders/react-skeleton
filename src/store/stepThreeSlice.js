import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'stepThreeData',
  initialState: {
    files: [],
  },
  reducers: {
    // name the action (as listed for useDispatch slice.actions)
    addStepThreeData: (state, action) => {
      state.files = action.payload.files;
    }
  }
});

// for use with useSelector()
export const getStepThreeData = (state) => state.stepThreeData.files;

// for use with useDispatch()
export const { addStepThreeData } = slice.actions;

export default slice.reducer;
