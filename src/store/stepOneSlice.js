import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'stepOneData',
  initialState: {
    firstName: '',
    lastName: '',
  },
  reducers: {
    // name the action (as listed for useDispatch slice.actions)
    addStepOneData: (state, action) => {
      state.firstName = action.payload.firstName;
      state.lastName = action.payload.lastName;
    }
  }
});

// for use with useSelector()
export const getStepOneData = (state) => state.stepOneData;

// for use with useDispatch()
export const { addStepOneData } = slice.actions;

export default slice.reducer;