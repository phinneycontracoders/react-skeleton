import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'stepTwoData',
  initialState: {
    email: '',
    hasPhone: false,
    phoneNumber: '+1', // country code of USA is +1, NOTE: but this could / should be handled as a number? - NOTE: libphonenumber-js uses parsePhoneNumberFromString
  },
  reducers: {
    // name the action (as listed for useDispatch slice.actions)
    addStepTwoData: (state, action) => {
      state.email = action.payload.email;
      state.hasPhone = action.payload.hasPhone;
      state.phoneNumber = action.payload.phoneNumber;
    }
  }
});

// for use with useSelector()
export const getStepTwoData = (state) => state.stepTwoData;

// for use with useDispatch()
export const { addStepTwoData } = slice.actions;

export default slice.reducer;